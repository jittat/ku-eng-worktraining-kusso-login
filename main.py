import random
import hashlib

from flask import Flask, redirect, abort, request
from authlib.integrations.flask_client import OAuth

app = Flask(__name__)
app.secret_key = 'fdjslkjfoiw4hfurwtuih45jpehdflrty4365456'

app.config['KUSSO_ACCESS_TOKEN_URL'] = 'https://sso.ku.ac.th/nidp/oauth/nam/token'
app.config['KUSSO_AUTHORIZE_URL'] = 'https://sso.ku.ac.th/nidp/oauth/nam/authz'
app.config['KUSSO_USER_PROFILE_URL'] = 'https://sso.ku.ac.th/nidp/oauth/nam/userinfo'
app.config['KUSSO_LOGOUT_URL'] = 'https://sso.ku.ac.th/nidp/app/logout'
app.config.from_object('settings')

oauth = OAuth(app)
oauth.register(
    'kusso',
    client_kwargs={
        'scope': 'basic',
        'token_endpoint_auth_method': 'client_secret_post',
    },
)

TARGETS = ['wt','wt-regis','wt-committee',
           'iupwt','iupwt-regis','iupwt-committee']

@app.route("/login/<target>/")
def login(target):
    if target not in TARGETS:
        target = 'wt'
    return oauth.kusso.authorize_redirect('https://wt.eng.ku.ac.th/wt/kusso/authorize/', state=target)


def generate_token(student_id, secret, nonce=''):
    if nonce == '':
        nonce = ''.join([random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for i in range(10)])
    key = nonce + student_id + secret
    key_hash = hashlib.sha1(key.encode('utf-8')).hexdigest()[:20]
    return student_id + '_' + nonce + '_' + key_hash


@app.route("/authorize/")
def authorize():
    client = oauth.create_client('kusso')
    token = client.authorize_access_token()
    target = request.args.get('state','')
    if target not in TARGETS:
        target = 'wt'

    # fetch user's profile
    resp = client.get(app.config['KUSSO_USER_PROFILE_URL'], token=token)
    if resp.status_code != 200:
        abort(500)

    user_info = resp.json()
    username = user_info['uid']

    secret = app.config['TOKEN_SECRETS'][target]
    role = app.config['TARGET_ROLES'][target]

    if (role == 'std') and (username.startswith('b')):
        username = username[1:]
        
    token = generate_token(username, secret)

    redirect_url = app.config['TARGET_REDIRECT_URLS'][target].format(token=token, role=role)

    return redirect(redirect_url)
